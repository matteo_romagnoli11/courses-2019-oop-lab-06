package it.unibo.oop.lab.collections1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;

/**
 * Example class using {@link java.util.List} and {@link java.util.Map}.
 * 
 */
public final class UseCollection {

    private UseCollection() {
    }

    /**
     * @param s
     *            unused
     */
    public static void main(final String... s) {
        /*
         * 1) Create a new ArrayList<Integer>, and populate it with the numbers
         * from 1000 (included) to 2000 (excluded).
         */
    	final ArrayList<Integer> array = new ArrayList<>();
    	for (Integer i = 1000; i <= 2000; i++) {
    		array.add(i);
    	}
        /*
         * 2) Create a new LinkedList<Integer> and, in a single line of code
         * without using any looping construct (for, while), populate it with
         * the same contents of the list of point 1.
         */
    	final LinkedList<Integer> list = new LinkedList<>();
    	list.addAll(array);
        /*
         * 3) Using "set" and "get" and "size" methods, swap the first and last
         * element of the first list. You can not use any "magic number".
         * (Suggestion: use a temporary variable)
         */
    	int tmp = array.get(0);
    	array.set(0, array.get(array.size()-1));
    	array.set(array.size()-1, tmp);
        /*
         * 4) Using a single for-each, print the contents of the arraylist.
         */
    	for (Integer i : array) {
    		System.out.println(i+" ");
    	}
        /*
         * 5) Measure the performance of inserting new elements in the head of
         * the collection: measure the time required to add 100.000 elements as
         * first element of the collection for both ArrayList and LinkedList,
         * using the previous lists. In order to measure times, use as example
         * TestPerformance.java.
         */
    	final ArrayList<Integer> arrayTest = new ArrayList<>();
    	final int ELEMS_TO_ADD = 100_000;
    	final int TO_MS = 1_000_000;
    	long timeArray = System.nanoTime();
    	
    	for (int i = 1; i <= ELEMS_TO_ADD; i++) {
            arrayTest.add(0, i);
        }
    	
    	timeArray = System.nanoTime() - timeArray;
    	
    	System.out.println("Adding " + ELEMS_TO_ADD
                + " int into the first position of the array list took " + timeArray
                + "ns (" + timeArray / TO_MS + "ms)");
    	

    	final LinkedList<Integer> listTest = new LinkedList<>();
    	long timeList = System.nanoTime();
    	
    	for (int i = 1; i <= ELEMS_TO_ADD; i++) {
    		listTest.add(0, i);
    	}
    	
    	timeList = System.nanoTime() - timeList;
    	
    	System.out.println("Adding " + ELEMS_TO_ADD
                + " int into the first position of the linked list took " + timeList
                + "ns (" + timeList / TO_MS + "ms)");
        /*
         * 6) Measure the performance of reading 1000 times an element whose
         * position is in the middle of the collection for both ArrayList and
         * LinkedList, using the collections of point 5. In order to measure
         * times, use as example TestPerformance.java.
         */
    	final int NUMBER_OF_READS = 1_000;
    	timeArray = System.nanoTime();
    	
    	for (int i = 1; i <= NUMBER_OF_READS; i++) {
    		arrayTest.get(array.size()/2);
    	}
    	
    	timeArray = System.nanoTime() - timeArray;
    	
    	System.out.println("Reading " + NUMBER_OF_READS
                + " times the element in the middle of the array list took " + timeArray
                + "ns (" + timeArray / TO_MS + "ms)");
    	
    	timeList = System.nanoTime();
    	
    	for (int i = 1; i <= NUMBER_OF_READS; i++) {
    		listTest.get(list.size()/2);
    	}
    	
    	timeList = System.nanoTime() - timeList;
    	
    	System.out.println("Reading " + NUMBER_OF_READS
                + " times the element in the middle of the linked list took " + timeList
                + "ns (" + timeList / TO_MS + "ms)");
        /*
         * 7) Build a new Map that associates to each continent's name its
         * population:
         * 
         * Africa -> 1,110,635,000
         * 
         * Americas -> 972,005,000
         * 
         * Antarctica -> 0
         * 
         * Asia -> 4,298,723,000
         * 
         * Europe -> 742,452,000
         * 
         * Oceania -> 38,304,000
         */
    	HashMap<String, Long> map = new HashMap<>();
    	map.put("Africa", 1_110_635_000L);
    	map.put("Americas", 972_005_000L);
    	map.put("Antarctica", 0L);
    	map.put("Asia", 4_298_723_000L);
    	map.put("Europe", 742_452_000L);
    	map.put("Oceania", 38_304_000L);
        /*
         * 8) Compute the population of the world
         */
    	long population = 0;
    	
    	for (final Long l : map.values()) {
    		population = population+l;
    	}
    	
    	System.out.println("Population: "+population);
    }
}
